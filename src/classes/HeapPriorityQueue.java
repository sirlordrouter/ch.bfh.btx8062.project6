package classes;

import java.util.Arrays;

import exceptions.EmptyPriorityQueueException;
import exceptions.NotComparableException;
import interfaces.Comparator;
import interfaces.PriorityQueue;

/**
 * Berner Fachhochschule</br> Medizininformatik BSc</br> Modul 8051-HS2012</br>
 * 
 * <p>
 * Describes a PriorityQueue based on an Heap. This Version needs not a special
 * Comparator handling the Items in this class. It only needs a Comparator for
 * the type of key used.
 * </p>
 * 
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @author Patrizia Zehnder, zehnp1@bfh.ch
 * @version 04-06-2013
 */
public class HeapPriorityQueue extends ArrayHeap implements PriorityQueue {

	protected Item[] H;
	

	/**
	 * An Item for storing Items containing a key and a value. The Item is then
	 * inserted into a heap, when inserting a new Item with insertItem(Object
	 * Key, Object Element).
	 * 
	 * @author Johannes Gnaegi, gnaegj1@bfh.ch
	 * @author Patrizia Zehnder, zehnp1@bfh.ch
	 * @version 23-05-2013
	 * 
	 */
	public class Item {

		private Object key;
		private Object value;

		public Item(Object key, Object value) {
			this.setKey(key);
			this.setValue(value);
		}

		/**
		 * @return the key
		 */
		public Object getKey() {
			return key;
		}

		/**
		 * @param key
		 *            the key to set
		 */
		public void setKey(Object key) {
			this.key = key;
		}

		/**
		 * @return the value
		 */
		public Object getValue() {
			return value;
		}

		/**
		 * @param value
		 *            the value to set
		 */
		public void setValue(Object value) {
			this.value = value;
		}

		@Override
		public String toString() {
			return "Item [key=" + key + ", value=" + value + "]";
		}

	}

	/**
	 * Constructor, initializes the PriorityQueue with a Comparator for sorting
	 * the Items in the Queue.
	 * 
	 * @param desc
	 *            if the Priority Queue is sorted descended
	 */
	public HeapPriorityQueue(Comparator c) {
		super(c);
		this.H = new Item[INITIAL_VALUE];
	}

	/**
	 * Inserts a new Item into the PriorityQueue
	 * 
	 * @param Key
	 *            a Key for sorting the items
	 * @param Element
	 *            an Element
	 */
	@Override
	public void insertItem(Object Key, Object Element)
			throws NotComparableException {
		Item newItem = new Item(Key, Element);
		// Compare the Same input to evaluate if comparable with given
		// Comparator, even if first element
		if (!c.isComparable(newItem.getKey(), newItem.getKey())) {
			throw new NotComparableException();
		}

		if (n == H.length - 1) {
			Item[] H2 = new Item[2 * H.length];
			for (int i = 1; i < H.length; i++) {
				H2[i] = H[i];
			}
			H = H2;
		}
		H[n + 1] = newItem;
		n++;
		upheap(n);
	}

	/**
	 * removes the item on the top.
	 * 
	 * @return {@code Item} removed
	 * @throws EmptyPriorityQueueException
	 *             if PriorityQueue is empty
	 */
	@Override
	public Object removeMin() throws EmptyPriorityQueueException {
		if (n == 0) {
			throw new EmptyPriorityQueueException();
		}
		Object min = H[1];
		H[1] = H[n];
		n--;
		downheap(1);
		return min;
	}

	/**
	 * returns the key from the top item.
	 * 
	 * @return the first key in the PriorityQueue
	 * @throws EmptyPriorityQueueException
	 *             if the PriorityQueue is empty
	 */
	@Override
	public Object minKey() throws EmptyPriorityQueueException {
		if (n == 0)
			throw new EmptyPriorityQueueException();
		return ((Item) H[1]).getKey();
	}

	/**
	 * returns the element from top item
	 * 
	 * @return the first element in the PriorityQueue
	 * @throws EmptyPriorityQueueException
	 *             if the PriorityQueue is empty
	 */
	@Override
	public Object minElement() throws EmptyPriorityQueueException {
		if (n == 0)
			throw new EmptyPriorityQueueException();
		return ((Item) H[1]).getValue();
	}

	private void upheap(int i) {
		int j = i / 2;
		if ((j > 0)
				&& c.isGreaterThan(((Item) H[j]).getKey(),
						((Item) H[i]).getKey())) {
			swap(i, j);
			upheap(j);
		}

	}

	private void downheap(int i) {

		int k = 0;
		int k1 = 2 * i;
		int k2 = 2 * i + 1;

		if (k2 <= n) {
			if (c.isGreaterThan(((Item) H[i]).getKey(), ((Item) H[k1]).getKey())
					|| c.isGreaterThan(((Item) H[i]).getKey(),
							((Item) H[k2]).getKey())) {
				if (c.isLessThan(((Item) H[k1]).getKey(),
						((Item) H[k2]).getKey())) {
					k = k1;
				} else {
					k = k2;
				}
			} // Do Nothing
		} else if (k1 <= n) {
			if (c.isGreaterThan(((Item) H[i]).getKey(), ((Item) H[k1]).getKey())) {
				k = k1;
			}
		}
		if (k != 0) {
			swap(i, k);
			downheap(k);
		}
	}

	protected void swap(int i, int j) {
		Item x = H[i];
		H[i] = H[j];
		H[j] = x;
	}

	@Override
	public String toString() {
		return "HeapPriorityQueueExt [c=" + c + ", n=" + n + ", H="
				+ Arrays.toString(H) + "]";
	}

}
