package classes;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

import exceptions.EmptyHeapException;
import exceptions.NotComparableException;
import interfaces.Comparator;
import interfaces.Heap;

/**
 * Berner Fachhochschule</br> Medizininformatik BSc</br> Modul 8051-HS2012</br>
 * 
 * <p>
 * Describes a Heap based on an Array.
 * </p>
 * 
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @author Patrizia Zehnder, zehnp1@bfh.ch
 * @version 23-05-2013
 */
public class ArrayHeap implements Heap {

	protected int n;
	protected Comparator c;
	protected Object[] H;
	protected static final int INITIAL_VALUE = 12;

	/**
	 * Constructor. Initializes the heap with a comparator for sorting items
	 * 
	 * @param c
	 *            a comparator for sorting the items in the heap
	 */
	public ArrayHeap(Comparator c) {
		this.n = 0;
		this.c = c;
		this.H = new Object[INITIAL_VALUE];
	}

	@Override
	/**
	 * returns the size of the heap
	 * @return size of the heap
	 */
	public int size() {
		return n;
	}

	@Override
	/**
	 * returns if the heap is empty or not
	 * @return if the heap is empty
	 */
	public boolean isEmpty() {
		return (n == 0);
	}

	/**
	 * inserts a new element at the end of the heap
	 * @param Element to insert into heap
	 * @throws NotComparableException if the element is not comparable with the 
	 * given Comparator
	 */
	@Override
	public void insertElement(Object Element) throws NotComparableException {
		if (!c.isComparable(Element, Element)) {
			throw new NotComparableException();
		}

		if (n == H.length - 1) {
			Object[] H2 = new Object[2 * H.length];
			for (int i = 1; i < H.length; i++) {
				H2[i] = H[i];
			}
			H = H2;
		}
		H[n + 1] = Element;
		n++;
		upheap(n);
	}

	/**
	 * removes the first element in the heap
	 * @return Element removed
	 * @throws EmptyHeapException if heap is empty
	 */
	@Override
	public Object removeMin() throws EmptyHeapException {
		if (n == 0) {
			throw new EmptyHeapException();
		}
		Object min = H[1];
		H[1] = H[n];
		n--;
		downheap(1);
		return min;
	}

	/**
	 * returns the first element in the tree
	 * @return First element in heap
	 * @throws EmptyHeapException if heap is empty
	 */
	@Override
	public Object minElement() throws EmptyHeapException {
		if (n == 0)
			throw new EmptyHeapException();
		return H[1];
	}

	/**
	 * Sorts the input of a given list.
	 * 
	 * @param collection
	 * @return the given Collection with sorted elements.
	 */
	public static <T extends Collection<Object>, S extends Comparator> T HeapSort(
			T collection, S comparator) {
		if (collection.isEmpty()) {
			return collection;
		}

		ArrayHeap heap = new ArrayHeap(comparator);
		Iterator<Object> i = collection.iterator();
		while (i.hasNext()) {
			heap.insertElement(i.next());
		}

		collection.clear();
		while (!heap.isEmpty()) {
			collection.add(heap.removeMin());
		}

		return collection;
	}

	private void upheap(int i) {
		int j = i / 2;
		if ((j > 0) && c.isGreaterThan(H[j], H[i])) {
			swap(i, j);
			upheap(j);
		}

	}

	private void downheap(int i) {

		int k = 0;
		int k1 = 2 * i;
		int k2 = 2 * i + 1;

		if (k2 <= n) {
			if (c.isGreaterThan(H[i], H[k1]) || c.isGreaterThan(H[i], H[k2])) {
				if (c.isLessThan(H[k1], H[k2])) {
					k = k1;
				} else {
					k = k2;
				}
			} // Do Nothing
		} else if (k1 <= n) {
			if (c.isGreaterThan(H[i], H[k1])) {
				k = k1;
			}
		}

		if (k != 0) {
			swap(i, k);
			downheap(k);
		}

	}

	private void swap(int i, int j) {
		Object x = H[i];
		H[i] = H[j];
		H[j] = x;
	}

	/**
	 * returns the height of the heap
	 * 
	 * @return height
	 */
	public int height() {
		return (int) (Math.log(n) / Math.log(2));
	}

	@Override
	public String toString() {
		return "ArrayHeap [n=" + n + ", c=" + c + ", H=" + Arrays.toString(H)
				+ "]";
	}

}
