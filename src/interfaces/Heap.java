package interfaces;

import exceptions.EmptyHeapException;
import exceptions.NotComparableException;

public interface Heap extends BasicCollection {
	
	public void insertElement(Object Element) throws NotComparableException;
	
	public Object removeMin() throws EmptyHeapException;
	
	public Object minElement() throws EmptyHeapException;

}
