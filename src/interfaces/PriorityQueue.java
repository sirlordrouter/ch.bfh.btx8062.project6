package interfaces;

import exceptions.EmptyPriorityQueueException;
import exceptions.NotComparableException;

public interface PriorityQueue extends BasicCollection {

	public void insertItem(Object Key, Object Element) throws NotComparableException;

	public Object removeMin() throws EmptyPriorityQueueException;

	public Object minKey() throws EmptyPriorityQueueException;

	public Object minElement() throws EmptyPriorityQueueException;

}
