package interfaces;
import exceptions.NotComparableException;

public interface Comparator {
	
	public boolean isLessThan(Object x, Object y) throws NotComparableException;
	
	public boolean isLessThanOrEqualTo(Object x, Object y) throws NotComparableException;

	public boolean isEqualTo(Object x, Object y) throws NotComparableException;

	public boolean isGreaterThan(Object x, Object y) throws NotComparableException;

	public boolean isGreaterThanOrEqualTo(Object x, Object y) throws NotComparableException;

	public boolean isComparable(Object x, Object y);

}
