package comparators;

import exceptions.NotComparableException;
import interfaces.Comparator;

/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br>
 * 
 *<p>Class Description</p>
 * TODO: schwierig wenn nicht klar ob Elemente Comparable sind => besonders f�r 
 * static Methode HeapSort in ArrayHeap
 *
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @author Patrizia Zehnder, zehnp1@bfh.ch
 * @version 23-05-2013
 */
public class AscendingIntegerComparator implements Comparator {

	@Override
	public boolean isLessThan(Object x, Object y) throws NotComparableException {
		if (!isComparable(x, y))
			throw new NotComparableException();
		int xvalue = (int) x;
		int yvalue = (int) y;
		return (xvalue < yvalue);
	}

	@Override
	public boolean isLessThanOrEqualTo(Object x, Object y)
			throws NotComparableException {
		if (!isComparable(x, y))
			throw new NotComparableException();
		int xvalue = (int) x;
		int yvalue = (int) y;
		return (xvalue <= yvalue);
	}

	@Override
	public boolean isEqualTo(Object x, Object y) throws NotComparableException {
		if (!isComparable(x, y))
			throw new NotComparableException();
		int xvalue = (int) x;
		int yvalue = (int) y;
		return (xvalue == yvalue);
	}

	@Override
	public boolean isGreaterThan(Object x, Object y)
			throws NotComparableException {
		if (!isComparable(x, y))
			throw new NotComparableException();
		int xvalue = (int) x;
		int yvalue = (int) y;
		return (xvalue > yvalue);
	}

	@Override
	public boolean isGreaterThanOrEqualTo(Object x, Object y)
			throws NotComparableException {
		if (!isComparable(x, y))
			throw new NotComparableException();
		int xvalue = (int) x;
		int yvalue = (int) y;
		return (xvalue >= yvalue);
	}

	@Override
	public boolean isComparable(Object x, Object y) {
		boolean r = false;
		if ((x instanceof Integer) & (y instanceof Integer))
			r = true;
		return r;
	}

}
