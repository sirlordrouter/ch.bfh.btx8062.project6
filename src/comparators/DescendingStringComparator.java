package comparators;

import exceptions.NotComparableException;
import interfaces.Comparator;

public class DescendingStringComparator implements Comparator{

	@Override
	public boolean isLessThan(Object x, Object y) throws NotComparableException {
		if (!isComparable(x, y))
			throw new NotComparableException();
		String xval = (String) x;
		String yval = (String) y;
		return (0 < xval.compareTo(yval));
	}

	@Override
	public boolean isLessThanOrEqualTo(Object x, Object y)
			throws NotComparableException {
		if (!isComparable(x, y))
			throw new NotComparableException();
		String xval = (String) x;
		String yval = (String) y;
		return (0 <= xval.compareTo(yval));
	}

	@Override
	public boolean isEqualTo(Object x, Object y) throws NotComparableException {
		if (!isComparable(x, y))
			throw new NotComparableException();
		String xval = (String) x;
		String yval = (String) y;
		return (0 == xval.compareTo(yval));
	}

	@Override
	public boolean isGreaterThan(Object x, Object y)
			throws NotComparableException {
		if (!isComparable(x, y))
			throw new NotComparableException();
		String xval = (String) x;
		String yval = (String) y;
		return (0 > xval.compareTo(yval));
	}

	@Override
	public boolean isGreaterThanOrEqualTo(Object x, Object y)
			throws NotComparableException {
		if (!isComparable(x, y))
			throw new NotComparableException();
		String xval = (String) x;
		String yval = (String) y;
		return (0 >= xval.compareTo(yval));
	}

	@Override
	public boolean isComparable(Object x, Object y) {
		boolean r = false;
		if ((x instanceof String) & (y instanceof String))
			r = true;
		return r;
	}
}
