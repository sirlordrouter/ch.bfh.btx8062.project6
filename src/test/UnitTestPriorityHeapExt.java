package test;

import static org.junit.Assert.*;

import org.junit.Test;

import comparators.AscendingIntegerComparator;
import classes.HeapPriorityQueue;
import classes.HeapPriorityQueue.Item;

public class UnitTestPriorityHeapExt {
	
	HeapPriorityQueue hpq = new HeapPriorityQueue(new AscendingIntegerComparator());

	@Test
	public void testHeapPriorityQueueExt() {
		assertNotNull(hpq);
	}

	@Test
	public void testSize() {
		
		hpq.insertItem(1, "1. Element");
		hpq.insertItem(3, "2. Element");
		hpq.insertItem(2, "3. Element");
		hpq.insertItem(4, "4. Element");
		assertTrue(hpq.size() == 4);
	}

	@Test
	public void testIsEmpty() {
		assertTrue(hpq.isEmpty());
		
		hpq.insertItem(1, "1. Element");
		hpq.insertItem(3, "2. Element");
		hpq.insertItem(2, "3. Element");
		hpq.insertItem(4, "4. Element");
		assertTrue(!hpq.isEmpty());
	}


	@Test
	public void testRemoveMin() {
		
		hpq.insertItem(1, "1. Element");
		hpq.insertItem(3, "2. Element");
		hpq.insertItem(2, "3. Element");
		hpq.insertItem(4, "4. Element");
		
		
		assertTrue((int)((Item)hpq.removeMin()).getKey() == 1);
	}

	@Test
	public void testMinKey() {
		hpq.insertItem(1, "1. Element");
		hpq.insertItem(3, "2. Element");
		hpq.insertItem(2, "3. Element");
		hpq.insertItem(4, "4. Element");
		
		assertTrue((int)(hpq.minKey()) == 1);
	}

	@Test
	public void testMinElement() {
		hpq.insertItem(1, "1. Element");
		hpq.insertItem(3, "2. Element");
		hpq.insertItem(2, "3. Element");
		hpq.insertItem(4, "4. Element");
		
		assertTrue(((String)(hpq.minElement())).equals("1. Element"));
	}


}
