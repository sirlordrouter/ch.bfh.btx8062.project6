package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import junit.framework.Assert;

import org.junit.Test;

import comparators.AscendingIntegerComparator;

import classes.ArrayHeap;

public class UnitTestHeap {
	
	ArrayHeap heap = new ArrayHeap(new AscendingIntegerComparator());

	@Test
	public void testArrayHeap() {
		assertNotNull(heap);
		
		heap.insertElement(2);
		heap.insertElement(6);
		heap.insertElement(7);
		heap.insertElement(3);
		heap.insertElement(1);
		heap.insertElement(5);
		heap.insertElement(9);
		heap.insertElement(4);
		heap.insertElement(10);
		heap.insertElement(8);
	}

	@Test
	public void testSize() {
		heap.insertElement(2);
		heap.insertElement(6);
		heap.insertElement(7);
		heap.insertElement(3);
		heap.insertElement(1);
		heap.insertElement(5);
		heap.insertElement(9);
		heap.insertElement(4);
		heap.insertElement(10);
		heap.insertElement(8);
		Assert.assertTrue(heap.size() == 10);
	}

	@Test
	public void testIsEmpty() {
		heap.insertElement(2);
		heap.insertElement(6);
		heap.insertElement(7);
		heap.insertElement(3);
		heap.insertElement(1);
		heap.insertElement(5);
		heap.insertElement(9);
		heap.insertElement(4);
		heap.insertElement(10);
		heap.insertElement(8);
		assertTrue(!heap.isEmpty());
	}

	@Test
	public void testRemoveMin() {
		heap.insertElement(2);
		heap.insertElement(6);
		heap.insertElement(7);
		heap.insertElement(3);
		heap.insertElement(1);
		heap.insertElement(5);
		heap.insertElement(9);
		heap.insertElement(4);
		heap.insertElement(10);
		heap.insertElement(8);
		assertTrue((int)heap.removeMin() == 1);
	}

	@Test
	public void testMinElement() {
		heap.insertElement(2);
		heap.insertElement(6);
		heap.insertElement(7);
		heap.insertElement(3);
		heap.insertElement(1);
		heap.insertElement(5);
		heap.insertElement(9);
		heap.insertElement(4);
		heap.insertElement(10);
		heap.insertElement(8);
		assertTrue((int)heap.minElement() == 1);
	}

	@Test
	public void testHeapSort() {
		ArrayList<Object> integerList = new ArrayList<Object>();
		integerList.add(12);
		integerList.add(100);
		integerList.add(4);
		integerList.add(56);
		integerList.add(23);
		integerList.add(144);
		integerList.add(18);
		integerList.add(71);
		integerList.add(20);
		integerList.add(12);
		integerList = ArrayHeap.HeapSort(integerList, new AscendingIntegerComparator());
		assertArrayEquals(new Object[] {4,12,12,18,20,23,56,71,100,144}, integerList.toArray());
	}

}
