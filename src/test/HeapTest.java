package test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import javax.swing.plaf.basic.BasicTreeUI.TreeCancelEditingAction;

import comparators.AscendingIntegerComparator;
import comparators.AscendingStringComparator;
import comparators.DescendingIntegerComparator;
import comparators.DescendingStringComparator;

import classes.*;
import exceptions.EmptyHeapException;
import exceptions.EmptyPriorityQueueException;
import exceptions.NotComparableException;

/**
 * Berner Fachhochschule</br> Medizininformatik BSc</br> Modul 8051-HS2012</br>
 * 
 * <p>
 * Testet die Implementation eines ArrayHeaps.
 * </p>
 * 
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @author Patrizia Zehnder, zehnp1@bfh.ch
 * @version 23-05-2013
 */
public class HeapTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		/************************* AscendingIntegerHeap ******************************/
		System.out.println("For each step the time is counted separatly");
		long start = System.nanoTime();
		System.out.println();
		System.out.println();
		System.out
				.println("/************************* AscendingIntegerHeap ******************************/");
		System.out.println("Instantiate class");
		ArrayHeap heap = new ArrayHeap(new AscendingIntegerComparator());
		System.out.println("Inserting Elements: 2,6,7,3,1,5,9,4,10,8");
		heap.insertElement(2);
		heap.insertElement(6);
		heap.insertElement(7);
		heap.insertElement(3);
		heap.insertElement(1);
		heap.insertElement(5);
		heap.insertElement(9);
		heap.insertElement(4);
		heap.insertElement(10);
		heap.insertElement(8);
		System.out.println("Heap: " + heap.toString());

		System.out.println("First Element: " + heap.minElement());
		System.out.println("Size: " + heap.size());

		System.out.println("Remove min");
		heap.removeMin();

		System.out.println("First Element: " + heap.minElement());
		System.out.println("Size: " + heap.size());

		System.out.println("Remove min 3 times");
		heap.removeMin();
		heap.removeMin();
		heap.removeMin();

		System.out.println("First Element: " + heap.minElement());
		System.out.println("Size: " + heap.size());
		System.out.println("TIME ELAPSED: "
				+ ((System.nanoTime() - start) / 1000) + "us");
		start = System.nanoTime();

		System.out.println();
		System.out.println();
		System.out
				.println("Produzieren von Fehlern um das Fehlerhandling zu kontrollieren (Nur in diesem Schritt): ");
		System.out.println("Removing all elements");
		while (!heap.isEmpty()) {
			heap.removeMin();
		}

		System.out.println("Size of: " + heap.size());
		System.out.println("isEmpty(): " + heap.isEmpty());

		try {
			System.out.println("Min element: " + heap.minElement());
		} catch (EmptyHeapException e) {
			System.out
					.println("Error in getting min element, heap is empty now");
		}
		try {
			System.out.println("Remove min: " + heap.removeMin());
		} catch (EmptyHeapException e) {
			System.out
					.println("Error in getting removing min, heap is empty now");
		}
		try {
			System.out.println("Inserting not comparable items: ");
			heap.insertElement("Fehler");
		} catch (NotComparableException e) {
			System.out
					.println("Error inserting two Strings, not comparable with Integer Comparator");
		}
		System.out.println("TIME ELAPSED: "
				+ ((System.nanoTime() - start) / 1000) + "us");
		start = System.nanoTime();

		/************************* AscendingIntegerHeap ******************************/
		System.out.println();
		System.out.println();

		/************************* DescendingIntegerHeap ******************************/
		System.out
				.println("/************************* DescendingIntegerHeap ******************************/");
		System.out.println("Instantiate class");
		ArrayHeap heap2 = new ArrayHeap(new DescendingIntegerComparator());

		System.out.println("Inserting Elements: 2,6,7,3,1,5,9,4,10,8");
		heap2.insertElement(2);
		heap2.insertElement(6);
		heap2.insertElement(7);
		heap2.insertElement(3);
		heap2.insertElement(1);
		heap2.insertElement(5);
		heap2.insertElement(9);
		heap2.insertElement(4);
		heap2.insertElement(10);
		heap2.insertElement(8);
		System.out.println("Heap: " + heap2.toString());

		System.out.println("First Element: " + heap2.minElement());
		System.out.println("Size: " + heap2.size());

		System.out.println("Remove min");
		heap2.removeMin();

		System.out.println("First Element: " + heap2.minElement());
		System.out.println("Size: " + heap2.size());

		System.out.println("Remove min 3 times");
		heap2.removeMin();
		heap2.removeMin();
		heap2.removeMin();

		System.out.println("First Element: " + heap2.minElement());
		System.out.println("Size: " + heap2.size());
		System.out.println("TIME ELAPSED: "
				+ ((System.nanoTime() - start) / 1000) + "us");
		start = System.nanoTime();
		/************************* DescendingIntegerHeap ******************************/
		System.out.println();
		System.out.println();
		/************************* AscendingStringHeap ******************************/
		System.out
				.println("/************************* AscendingStringHeap ******************************/");
		System.out.println("Instantiate class");
		ArrayHeap heap3 = new ArrayHeap(new AscendingStringComparator());

		System.out.println("Inserting Elements: F,A,C,H,D,J,E,I,F,B");
		heap3.insertElement("F");
		heap3.insertElement("A");
		heap3.insertElement("C");
		heap3.insertElement("H");
		heap3.insertElement("D");
		heap3.insertElement("J");
		heap3.insertElement("E");
		heap3.insertElement("I");
		heap3.insertElement("F");
		heap3.insertElement("B");
		System.out.println("Heap: " + heap3.toString());

		System.out.println("First Element: " + heap3.minElement());
		System.out.println("Size: " + heap3.size());

		System.out.println("Remove min");
		heap3.removeMin();

		System.out.println("First Element: " + heap3.minElement());
		System.out.println("Size: " + heap3.size());

		System.out.println("Remove min 3 times");
		heap3.removeMin();
		heap3.removeMin();
		heap3.removeMin();

		System.out.println("First Element: " + heap3.minElement());
		System.out.println("Size: " + heap3.size());
		System.out.println("TIME ELAPSED: "
				+ ((System.nanoTime() - start) / 1000) + "us");
		start = System.nanoTime();
		/************************* AscendingStringHeap ******************************/
		System.out.println();
		System.out.println();
		/************************* DescendingStringHeap ******************************/
		System.out
				.println("/************************* DescendingStringHeap ******************************/");
		System.out.println("Instantiate class");
		ArrayHeap heap4 = new ArrayHeap(new DescendingStringComparator());

		System.out.println("Inserting Elements: F,A,C,H,D,J,E,I,F,B");
		heap4.insertElement("F");
		heap4.insertElement("A");
		heap4.insertElement("C");
		heap4.insertElement("H");
		heap4.insertElement("D");
		heap4.insertElement("J");
		heap4.insertElement("E");
		heap4.insertElement("I");
		heap4.insertElement("F");
		heap4.insertElement("B");
		System.out.println("Heap: " + heap4.toString());

		System.out.println("First Element: " + heap4.minElement());
		System.out.println("Size: " + heap4.size());

		System.out.println("Remove min");
		heap4.removeMin();

		System.out.println("First Element: " + heap4.minElement());
		System.out.println("Size: " + heap4.size());

		System.out.println("Remove min 3 times");
		heap3.removeMin();
		heap3.removeMin();
		heap3.removeMin();

		System.out.println("First Element: " + heap4.minElement());
		System.out.println("Size: " + heap4.size());
		System.out.println("TIME ELAPSED: "
				+ ((System.nanoTime() - start) / 1000) + "us");
		start = System.nanoTime();
		/************************* DescendingStringHeap ******************************/
		System.out.println();
		System.out.println();
		/************************* Sort Heap ******************************/
		ArrayList<Object> integerList = new ArrayList<Object>();
		integerList.add(12);
		integerList.add(100);
		integerList.add(4);
		integerList.add(56);
		integerList.add(23);
		integerList.add(144);
		integerList.add(18);
		integerList.add(71);
		integerList.add(20);
		integerList.add(12);
		System.out.println("Integer Liste: " + integerList.toString());

		start = System.nanoTime();
		System.out.println("Integer Liste sortieren: ASC");
		integerList = ArrayHeap.HeapSort(integerList,
				new AscendingIntegerComparator());
		System.out.println("Integer Liste: " + integerList.toString());
		System.out.println("TIME ELAPSED: "
				+ ((System.nanoTime() - start) / 1000) + "us");
		start = System.nanoTime();

		System.out.println("Integer Liste sortieren: DESC");
		integerList = ArrayHeap.HeapSort(integerList,
				new DescendingIntegerComparator());
		System.out.println("Integer Liste: " + integerList.toString());
		System.out.println("TIME ELAPSED: "
				+ ((System.nanoTime() - start) / 1000) + "us");
		/************************* Sort Heap ******************************/
		System.out.println();
		System.out.println();

		// TODO: Add with Random 1000 Items, compare with ArrayList
		ArrayHeap h10 = new ArrayHeap(new AscendingIntegerComparator());
		ArrayList al = new ArrayList();

		Random randomGenerator = new Random();
		System.out.println("Inserting 1000 Items in an ArrayList (Range 0-10000)");
		start = System.nanoTime();
		
		for (int i = 1; i <= 1000; ++i) {
			int randomInt = randomGenerator.nextInt(10000);
			al.add(randomInt);
		}
		System.out.println("TIME ELAPSED: "
				+ ((System.nanoTime() - start) / 1000) + "us");
		
		System.out.println("Inserting 1000 Items in an Heap(Range 0-10000)");
		start = System.nanoTime();

		for (int i = 1; i <= 1000; ++i) {
			int randomInt = randomGenerator.nextInt(10000);
			h10.insertElement(randomInt);
		}
		System.out.println("TIME ELAPSED: "
				+ ((System.nanoTime() - start) / 1000) + "us");
		
		System.out.println("Reversing Order in ArrayList with Collections-Class");
		start = System.nanoTime();	
		Collections.reverse(al);
		al.toString();
		System.out.println("TIME ELAPSED: "
				+ ((System.nanoTime() - start) / 1000) + "us");
		
		System.out.println("Reversing Order in ArrayList with HeapSort");
		start = System.nanoTime();	
		al = ArrayHeap.HeapSort(al,
				new AscendingIntegerComparator());
		al.toString();
		System.out.println("TIME ELAPSED: "
				+ ((System.nanoTime() - start) / 1000) + "us");

	}

}
