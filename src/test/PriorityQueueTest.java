package test;

import comparators.AscendingIntegerComparator;
import comparators.DescendingIntegerComparator;
import classes.HeapPriorityQueue;
import exceptions.EmptyPriorityQueueException;
import exceptions.NotComparableException;

/**
 * Berner Fachhochschule</br> Medizininformatik BSc</br> Modul 8051-HS2012</br>
 * 
 * <p>
 * Testet die Implementierung eines HeapPriorityQueue.
 * 
 * </p>
 * 
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @author Patrizia Zehnder, zehnp1@bfh.ch
 * @version 29-05-2013
 */
public class PriorityQueueTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		 * Testet nur mit Comparatoren f�r Integer als Keys.
		 */
		/********************************** Ascending HPQ ***************************************/
		HeapPriorityQueue hpq3 = new HeapPriorityQueue(
				new AscendingIntegerComparator());
		hpq3.isEmpty();
		hpq3.insertItem(4, "4");
		hpq3.insertItem(12, "12");
		hpq3.insertItem(6, "6");
		hpq3.insertItem(21, "21");
		hpq3.insertItem(22, "22");
		hpq3.insertItem(11, "11");
		hpq3.insertItem(3, "3");
		hpq3.insertItem(6, "6");
		hpq3.insertItem(1, "1");
		hpq3.insertItem(12, "12");
		System.out.println("Heap Priority Queue 3: " + hpq3.toString());
		System.out.println("Size of: " + hpq3.size());
		System.out.println("isEmpty(): " + hpq3.isEmpty());
		System.out.println("Min element: " + hpq3.minElement());
		System.out.println("Min key: " + hpq3.minKey());
		System.out.println();

		System.out.println("Remove min");
		hpq3.removeMin();
		System.out.println("Size of: " + hpq3.size());
		System.out.println("isEmpty(): " + hpq3.isEmpty());
		System.out.println("Min element: " + hpq3.minElement());
		System.out.println("Min key: " + hpq3.minKey());
		System.out.println();

		System.out.println("Remove min 3 times");
		hpq3.removeMin();
		hpq3.removeMin();
		hpq3.removeMin();
		System.out.println("Size of: " + hpq3.size());
		System.out.println("isEmpty(): " + hpq3.isEmpty());
		System.out.println("Min element: " + hpq3.minElement());
		System.out.println("Min key: " + hpq3.minKey());
		System.out.println();

		System.out.println("Removing all elements");
		while (!hpq3.isEmpty()) {
			hpq3.removeMin();
		}

		System.out.println("Size of: " + hpq3.size());
		System.out.println("isEmpty(): " + hpq3.isEmpty());

		try {
			System.out.println("Min element: " + hpq3.minElement());
		} catch (EmptyPriorityQueueException e) {
			System.out
					.println("Error in getting min element, heap is empty now");
		}
		try {
			System.out.println("Min key: " + hpq3.minKey());
		} catch (EmptyPriorityQueueException e) {
			System.out
					.println("Error in getting min key, heap is empty now");
		}
		try {
			System.out.println("Remove min: " + hpq3.removeMin());
		} catch (EmptyPriorityQueueException e) {
			System.out
					.println("Error in getting removing min, heap is empty now");
		}
		try {
			System.out.println("Inserting not comparable items: ");
			hpq3.insertItem("Fehler", "Error");
		} catch (NotComparableException e) {
			System.out
					.println("Error inserting two Strings, not comparable");
		}
		/********************************** Ascending HPQ ***************************************/
		System.out.println();
		System.out.println();
		/********************************** Descending HPQ ***************************************/
		HeapPriorityQueue hpq4 = new HeapPriorityQueue(
				new DescendingIntegerComparator());
		hpq4.insertItem(4, "4");
		hpq4.insertItem(12, "12");
		hpq4.insertItem(6, "6");
		hpq4.insertItem(21, "21");
		hpq4.insertItem(22, "22");
		hpq4.insertItem(11, "11");
		hpq4.insertItem(3, "3");
		hpq4.insertItem(6, "12");
		hpq4.insertItem(1, "1");
		hpq4.insertItem(12, "12");
		System.out.println("Heap Priority Queue 4: " + hpq4.toString());
		System.out.println("Size of: " + hpq4.size());
		System.out.println("isEmpty(): " + hpq4.isEmpty());
		System.out.println("Min element: " + hpq4.minElement());
		System.out.println("Min key: " + hpq4.minKey());
		System.out.println();

		System.out.println("Remove min");
		hpq4.removeMin();
		System.out.println("Size of: " + hpq4.size());
		System.out.println("isEmpty(): " + hpq4.isEmpty());
		System.out.println("Min element: " + hpq4.minElement());
		System.out.println("Min key: " + hpq4.minKey());
		System.out.println();

		System.out.println("Remove min 3 times");
		hpq4.removeMin();
		hpq4.removeMin();
		hpq4.removeMin();
		System.out.println("Size of: " + hpq4.size());
		System.out.println("isEmpty(): " + hpq4.isEmpty());
		System.out.println("Min element: " + hpq4.minElement());
		System.out.println("Min key: " + hpq4.minKey());
		System.out.println();

		System.out.println("Removing all elements");
		while (!hpq4.isEmpty()) {
			hpq4.removeMin();
		}

		System.out.println("Size of: " + hpq4.size());
		System.out.println("isEmpty(): " + hpq4.isEmpty());

		try {
			System.out.println("Min element: " + hpq4.minElement());
		} catch (EmptyPriorityQueueException e) {
			System.out
					.println("Error in getting min element, heap is empty now");
		}
		try {
			System.out.println("Min key: " + hpq4.minKey());
		} catch (EmptyPriorityQueueException e) {
			System.out
					.println("Error in getting min key, heap is empty now");
		}
		try {
			System.out.println("Remove min: " + hpq4.removeMin());
		} catch (EmptyPriorityQueueException e) {
			System.out
					.println("Error in getting removing min, heap is empty now");
		}
		try {
			System.out.println("Inserting not comparable items: ");
			hpq4.insertItem("Fehler", "Error");
		} catch (NotComparableException e) {
			System.out
					.println("Error inserting two Strings, not comparable");
		}
		/********************************** Descending HPQ ***************************************/

	}

}
